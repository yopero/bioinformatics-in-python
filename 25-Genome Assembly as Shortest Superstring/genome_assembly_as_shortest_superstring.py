from pathlib import Path
from utilities import parse_fasta_file
from collections import namedtuple
from itertools import permutations

Overlap = namedtuple('Overlap', ['start', 'end', 'length'])


def find_overlap(first, second):
    """
    Finds an overlapping substrings between two words
    >>> find_overlap('magnificent', 'challenge')
    Overlap(start='magnificent', end='challenge', length=0)
    >>> find_overlap('apple', 'pleasure')
    Overlap(start='apple', end='pleasure', length=3)
    >>> find_overlap('erratic', 'player')
    Overlap(start='player', end='erratic', length=2)
    >>> find_overlap('payee', 'eerily')
    Overlap(start='payee', end='eerily', length=2)
    >>> find_overlap('eeriest', 'estimatee')
    Overlap(start='eeriest', end='estimatee', length=3)
    >>> find_overlap('estimatee', 'eeriest')
    Overlap(start='eeriest', end='estimatee', length=3)
    >>> find_overlap('suren', 'pleasure')
    Overlap(start='pleasure', end='suren', length=4)
    >>> find_overlap('sure', 'pleasure')
    Overlap(start='pleasure', end='sure', length=4)
    """
    max_overlap = min(len(first), len(second))
    overlap = None
    for i in range(0, max_overlap+1):
        if first[-i:] == second[:i]:
            overlap = Overlap(start=first, end=second, length=i)
        elif second[-i:] == first[:i]:
            overlap = Overlap(start=second, end=first, length=i)
        elif overlap:
            return overlap
    if overlap:
        return overlap

    return Overlap(start=first, end=second, length=0)

def combine_overlap(overlap):
    """
    >>> combine_overlap(Overlap(start='clap', end='apple', length=2))
    'clapple'
    """
    return f'{overlap.start}{overlap.end[overlap.length:]}'

def combine_words(words):
    """
    Compresses words based on overlaps.
    >>> combine_words(['magnificent', 'challenge'])
    'challengemagnificent'
    >>> combine_words(['apple', 'pleasure'])
    'appleasure'
    >>> combine_words(['apple', 'pleasure', 'surely'])
    'appleasurely'
    >>> combine_words(['pleasure', 'apple', 'rendered', 'clap', 'suren'])
    'clappleasurendered'
    """
    perms = permutations(words)
    shortest = None
    for permutation in perms:
        combined = ''
        permutation = list(permutation)
        while next_word := permutation.pop() if permutation else False:
            overlap = find_overlap(combined, next_word)
            combined = combine_overlap(overlap)
        if not shortest or len(combined) < len(shortest):
            shortest = combined
    return shortest

def get_strings(file_path):
    """
    Return strings from fasta file
    >>> get_strings("test_input.fasta")
    ['ATTAGACCTG', 'CCTGCCGGAA', 'AGACCTGCCG', 'GCCGGAATAC']
    """
    fasta = parse_fasta_file(file_path)
    dna_strings = list(fasta.values())

    return dna_strings

def genome_assembly_as_shortest_superstring(file_path):
    """
    >>> genome_assembly_as_shortest_superstring("test_input.fasta")
    'ATTAGACCTGCCGCCGGAATACCTGCCGGAA'
    """
    dna_strings = get_strings(file_path)
    combined = combine_words(dna_strings)

    return combined

current_dir = Path(__file__).parent

test_file = current_dir / "test_input.fasta"
super_string = genome_assembly_as_shortest_superstring(test_file)
print(super_string)

import time
st = time.time()
data_file = current_dir / "input_genome_assembly_as_shortest_superstring.fasta"
super_string = genome_assembly_as_shortest_superstring(data_file)
et = time.time()
print(super_string)
elapsed_time = et - st
print('Execution time:', elapsed_time, 'seconds')
    # import doctest
    # doctest.testmod()

