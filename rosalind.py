from utilities import ROOT_DIR
from bio_seq import BioSeq
from collections import Counter
from math import factorial


def ros_counting_nucleotides():
    """Counting DNA Nucleotides"""
    sample_text = "AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC"
    sample = BioSeq(sample_text)
    expected = "20 12 17 21"
    result = sample.nucleotide_frequency(numbers_only=True)
    assert expected == result

    dataset = "GCCGGAGGGGAGGGAACAGGTTCTGTTTGCTCTGTGAAATCGCCGGCTACTACCGCGATAAGCGAGCGCCGGCGCGTTCTGTAAATAGCTCGGGCTGCTCGCCCTGTGCTTTTAACCAGAGATGAGCAACGCGATATTTGCAACCTCATTCTCCATCCCTTTCCTATCTAAATGCTCTATGGCCCATGGACGCGAGAGGTGATATTAAGCAGCTGAGGGAAGGCGTCCTCCAAACTATGAGGCAGACCGCAGCTCGTAACACAAGGCATCTAGGAATCCTACAGTGTCCGCCACCTACTACCCCCCCTGTACATTGACGTCACGTTTGCTCTCACCGTGCCACAGATGGCGGGAACTATCAGGCCAAACATCTATAAAGTACCACAGTTGGGATGCCACCTTATCCGTTCATGTTCTCGTTGAGAACCTTGGTTCCAATCACAGCCAGTATCATATCGCACGTACTGGTAAGGGCTAGCAAGGAGGGTAATACATAGGGAGTGGGTCCCCTTTGACTACGGGAAGTTTACACTTAATGTCAGGAGGCAGACCCATACTCATTTAAACTATAAGCAAGCGCCGACTACGGCCAAGCGCCGCTGTTTAACTTCAACCATCGATGAGTGAAAGCGAACTCCGGAGTTGCCGTTATCTGGACTTCGTCAAGCGAGCTGTATCCCTCAGCAGCGCTAATTATGATGCCTGCCCATTAAGCAGAGTTCAATCGAATGTAGTTTCTACTACCATCCCCAAAAAGCTACCAAGCGTCTACCCGCATTTCCTACTATCCATCAGAGTGCCTCCCATAGATAGCGGTAGAAATGCGGA"
    sample = BioSeq(dataset)
    print(sample.nucleotide_frequency(numbers_only=True))


def ros_transcribing_dna_to_rna():
    sample_text = "GATGGAACTTGACTACGTAAATT"
    expected = "GAUGGAACUUGACUACGUAAAUU"
    sample = BioSeq(sample_text)
    result = sample.transcription()
    assert expected == result

    dataset = "AGATGCAACCTACGCGTCATATCCTTGCAGGCCGTATCCCGACAGTGTGCACCTTCGAGCTTCACAATTGATCAGTGCTGCTACTAACTAGAATCTCTGAAAAGCACGGAACGGCCATTGACACCTGGGGGCCACTTTTGAGGTTACAGCAAGTTTATCCTCGCGACGGACGAGTCTTGAGGCTGTAGGCACTAGTGCGTTCGGAACCCCAGTGTCCCTTGCTGTCCTCGGCGACCAGTCAGTCGGGTAATTCAGCTCCATTTTCCTGCAAACGCAAATAATGAAGGCGGGGTAGCATTGATCACGATCGCAAAACCTGTCAGATGCGCATACCCGCTTACAGAAATCGGTACTAAGATGAGGAAGATGACCAGTCGATTGCAATCGAAAAATACCAGACGGAGCTCGGAGTGCCAAACTTGACGCTCTAGTCACCTCGAGCAGCTCGCAGAGCTACACGGATCCTTCCGAGTCAAGTTAACGGTACCCCTGTCTTTCTGCCTGCTATGAATGATACACGATACCGCCAGTTAAGGTATCCCGGTTAAACTGCATTGTCGGACCAGGCAAGGAAAATTCCCGCTAGTTTCACGCGCGCTAGCGACACCTATTTGCGTGGCCGTCCATTGGGTAAGAATAGTCACTGATTCTCGAGGCATGCAGTAGGTAGGAGGTGCAGAGAGTTGCTGGCTCGCCTCCGGCGCAAAGTGACGACAGACAAGTCACAATTGTTGGTACATGGTGTGGACTTTGGATCGTACTAGAGAATTGCATGGTAGACACCATAGATTCCACGCCAGCGCTTCCTTAAGCTCGACAAGCAAAAAGCGGAGCTGACCATTTCGGGACGACGCGCCACTGTTTGGTGATTCAAGGTCGAGAGGTAGCGTGAGACGCCACGTTCTCTCGCTCGCGTTCCTACTGCAGCCAGCGCCTTGACTATTAAGACAGTCTGCTTAAGGCCTACGCCTGCTCCCCTCCACG"
    sample = BioSeq(dataset)
    result = sample.transcription()
    print(result)


def ros_complementing_a_Strand_of_DNA():
    sample_text = "AAAACCCGGT"
    expected = "ACCGGGTTTT"
    sample = BioSeq(sample_text)
    result = sample.reverse_complement()
    assert expected == result

    dataset = "ACTAAGTCTACAAAGGACGGTTAATTGAATCCTGGGGGAATACAACCTGTATTATCGTCAAGTGCGCTTTGGATGTTGTCCATTTAGGAATGCAACTGATAGAAAAGACCATGCGCATGAAGTGACTTATGATATGTCACTAGCGTAGGGATATCGGCGCAGGAACATCCTTTCAAGACGGCGAAGTGGGGAAAGATTTCCGTGATCTCCTTTTGGTCACTAAGTGGGTTCGCCAACCTGACTCATCATTCAGACAGCACTTGTCTGTGGGAAGGGTAAGGAAGTGTGCACACGCTGCACGATTATTTGATGCCAGCTAGCCGGTCATGGAGGAACACGAAAGCTCCTTATCCACAGATCGTGTATGCAGTTAATCACGCAGGCACTCCAGCGGTCGCTGCTACGAAGCCTATATCCGTCAACCTGTCGGGTGCGCCCTTTCAGTCTTTGGCACAATCACATGCGTGCGATTCTAACATCGACGACATAGCGCTTCTTTCTCCCTGACATACCCGTTAGTAACAGGATCCTCAGAAAGTCCACGGTTGCGGAGGCGGGTACTGGAGCTGAGGTTTGGCTTCATTCCCTTTATCCGCACAAGTCTGATGCCAGTTCAAAGCTTCATCGGGTATACGCGCCACAGCCAGCGGACTGCTAGGGGCTAAAGTCCCCCGTTAACCTAGCTTGCGATAATAGCCATTAAACGTAAGAGAACCCTAGCACCCGCAGAAAATCTTGCCGTTCCCTTCGCCCTTGGAACCCGGGTAGTTATCGGGACGGGTCGCCGAACGATAGTCGCCGTTTGTTTGACAAGATCTGAATTGACCAGTGATCAGCAGAAAGGCACACCTCTGCTACGGTCGATGCCTGTCGTCGGAAGCCACCGGGTTGGCTGTGTGGTCGACTGTTCTTCCGTTTAATGCCGGGAAGCGTTCGTCGCCGACACCAACAGGCGCGGGCCCAGCACGAACACGCTCCATTCCTTACG"
    sample = BioSeq(dataset)
    result = sample.reverse_complement()
    print(result)


def ros_rabbits_mortal_calculation(months, age, offsprings=1):
    population = [(0, 1), (1, 0)]

    for i in range(2, months):
        parents_prev = population[i - 1][0]
        children = parents_prev * offsprings
        parents = sum(population[i - 1])
        if i - age >= 0:
            parents -= population[i - age][1]

        population.append((parents, children))

    return sum(population[-1])


def _ros_rabbits_calculation(months, offsprings):
    parents, children = 1, 1
    for _ in range(2, months):
        fn = parents + children * offsprings
        children, parents = parents, fn
    return fn


def ros_rabbits():
    months, offsprings = 5, 3
    expected = 19
    result = _ros_rabbits_calculation(months, offsprings)
    assert expected == result
    months, offsprings = 29, 5
    result = _ros_rabbits_calculation(months, offsprings)

    print(result)


def parse_fasta_file(filename):
    with open(filename) as fp:
        Lines = fp.readlines()
        DNA_strings = {}
        for line in Lines:
            line = line.strip()
            if line.startswith(">"):
                string_id = line.replace(">", "")
                DNA_strings[string_id] = ""
            else:
                DNA_strings[string_id] += line

    return DNA_strings


def ros_get_highest_gc_content(filename):
    dna_strings = parse_fasta_file(filename)
    results = {}
    for string_id, data in dna_strings.items():
        tmp = BioSeq(data)
        results[string_id] = tmp.gc_content()
        
    gc_results = {k: v for k, v in sorted(results.items(), key=lambda item: item[1], reverse=True)}
    for key, value in gc_results.items():
        print(key)
        print(value)
        print(10*"+")


def _ros_hamming_distance_calculate(string_a, string_b):
    result = sum([character_a != character_b for character_a, character_b in zip(string_a, string_b)])
    
    return result


def ros_hamming_distance():
    stra = "GAGCCTACTAACGGGAT"
    strb = "CATCGTAATGACGGCCT"
    expected = 7
    result = _ros_hamming_distance_calculate(stra, strb)
    assert expected == result

    stra = "GTATTGTGGGCGTTGACGATTTGTGTCGTAGGTCGCAACTTTATAAGGTACGAAAATTCAACCCTGGGGAATGTTATACAAAAGGCAAACTGTACCTCGCGCGGTCAGCACGGACTGGCCTAGCGCCTCCAGCATAGCCTGGTCAATGACATCTTAAAAACACAGTCATTTGGCCGAGGCTTTCTTAAAAACAAACAGGACCCAATGGCAATATAGGCGGTCCACATATGCCCGGTTGACCAACGAGGTAGTCTGGCGCGTAGCCTCATCATTTGAATAGCCGCGAGTCTATACGGAGCCGAAGGTGTGATGGGATCGATGGGCCCCCACCTTCGCGAAATGGGTACTATTAGGTCTTAATCGGTGAGTATACACCAAGGGGTGCTAGCCCAGGGATAAGTTTTGTTCCGCGTAGGGTTTGAAGCTGCACCTTGCGCAGGCTCTACTTAGAGTCACTCGGCGCTGGTATATCAGTCGTCGTTAACCCGGATTGAGGGGTAGAGCAAAGCTGCCTCCACTCTTCTCACTACGGGGGTTACCTGAGTCTCATGCTCCTAAATGCGTGTGCACTTATAAAGCCGTCAAGGTACGGGCGAGTGGAAAGGCACCACACCTTCCTTCGATAACCCCGTGTCCAGCTCCCGATAGTATTGCACTTCTCAGCATTAACAGAGTGGCCACCCCATGATCCAGATATCCGCATATGCGATAGCTGATAGACCTGCGCATGTAAGAACAATGCGGATCTTTAGGGCAATCGATAGAGCCTGTGATCGGCTCTACTAGTAGAGCATTCTGAGAAGTATGACTCTCCTTCCACGCTTCCCTTGGATATTTATTTAGTTGATTTTTGACGACTGCATGTTGCTCTTCGCAGCTCAGCAATAAAATAGACATTGTATGGAGGTGAAATTACGTCGCTCTCCAATAGCGCATAAGCCACC"
    strb = "GTCGTGTCTAAGCTGCGGATTTAGGGTCCAGCAATGAGCTTCAGAATGCCACGAGAGCGAACCCATGATAGGTCCATAAAACCGGCCAACGGTAAATCTACCGGGGAAGACGTAGCTTCCTTTCGCTGGCCGCAGATACTGTCCAATGACTATCCAACAACAGAGAAGTATCGCATGGTGTACGTTAGCAACCCACCTCCCTCAATGGCAACCTATCCTAATCACATACGAAGTGTAATCCCACGGGGTATTCGCGCGCGCAGCGCCATCTTTTGATGGGTACAGAGTGCAGCCGGAGTGGATACTAGGATAGTATAGGTGGACACATGCGAGTACGTAGTTGGCCCCATTAGATGTTATTTTGTGTCTATGAACCAAGGGATGCTACTCATGCGACACGTAATCGTCCCTCGAGGATCATACCCGGCTCCGTGAATATTGGTTTTTCATATGTTCAAGAACCTGTGATACCGGTGGTCCCTCATTCGGCGTATTGGGGAGTCGAAAAGTGACGCCACCAATATCGAGACGGTGCTGACCTTATTAAGATGCCCCCCACATCGTCCGAGTTTACGGACCTGCCACGGTACGGGAGAGTGGCGAGGCCCCTCACCCAACTTTGTTTACGCAGTGGCCTGCTTCCGATTGTTTGCGGCTTATCTATGTCACCCGCGTAGCCACTACGAGATCCTATCCGCCGGCTGTGAGATCGCTGATATACCTGGACGTTTAAAGCTCGTTCGTTTCTTAGTCGGAATCGATAAAACTTGGAATACACTGTCCTAGTAGTACGACGTTACAAAGGTTGCAAATCTGGGCAGCTGACATGCGCTAGTTTCATCATCAGATTTTGAAGCCTATGTGATGCTATCCGTAGCCCGCCGAGTCAACATGGCTCGCATCGTCGTACCTTTGCTACGGGTCCTATTTTATCATTAGCCCCT"


    print(_ros_hamming_distance_calculate(stra, strb))


def translating_rna_into_protein(input_file, output_file):
    mRNA = "AUGGCCAUGGCGCCCAGAACUGAGAUCAAUAGUACCCGUAUUAACGGGUGA"
    expected = "MAMAPRTEINSTRING"
    myseq = BioSeq(mRNA, "RNA")
    result = "".join(myseq.translate_seq()).replace("_", "")
    assert expected == result
    with open(input_file, 'r') as file:
        mRNA = file.read().replace('\n', '')
    myseq = BioSeq(mRNA, "RNA")
    result = "".join(myseq.translate_seq()).replace("_", "")
    f = open(output_file, "w+")
    f.write(result)
    f.close()
    print(result)


def find_motifs_as_indices(stack, needle):
    stack_lenght = len(stack)
    needle_lenght = len(needle)
    for i in range(stack_lenght - needle_lenght + 1):
        if stack[i:i + needle_lenght] == needle:
            yield i + 1


def rosalind_finding_a_motif_in_dna():
    genome_test = "GATATATGCATATACTT"
    motif_test = "ATAT"
    expected = [2, 4, 10]
    found = find_motifs_as_indices(genome_test, motif_test)
    result = [motif for motif in found]
    assert result == expected

    genome = "TGCCCAGAAGGACTAGGGTAGCTACTCGAACTGCCCAGTGCCCAGGTGTGCCCAGTGCCCAGCTGCCCAGATGCCCAGCCATGCCCAGTGCCCAGCCCACCTGCCCAGTAATGCCCAGAAACTAGGATGGCATGCCCAGCTTTTGCCCAGCCTGTGCCCAGTGCCCAGCTGCCCAGTGCCCAGCCATGCCCAGAGGAGGTGCCCAGCGTATGCCCAGCAGGACTAGAACACGTGGTCCTGAACGGTGCCCAGGTCGCAGTCAAGTGCCCAGATGCCCAGTGCCCAGTTGAATGCCCAGTGCCCAGATGCCCAGGCCTGCCCAGTGCCCAGGGTGCCCAGTGCCCAGTTTGCCCAGGTTGCCCAGATGCCCAGATGCCCAGCTGCCCAGATACCTGCCCAGTGCCCAGATGCCCAGTTGCCCAGTTGCCCAGGCCATGCCCAGTGCCCAGATGCCCAGATGCCCAGAATCTGGGTGCCCAGTGCCCAGGTGCCCAGCTTGCCCAGACTTGCCCAGACGCATTGCCCAGATGCCCAGCTGCCCAGTAAAGTGCCCAGTTTCCTAGGCTGCCCAGCTTGCCCAGTGCCCAGATGGGTGGGTGCCCAGATTGCCCAGTGCCCAGATCCAGTTTGCCCAGTGCCCAGGTGTTGCCCAGTGCCCAGTGCCCAGGTGAATGCCCAGTGCCCAGCATGCCCAGTTGCCCAGTTGCCCAGGGTCGTTGCCCAGTTGCCCAGTAATTAGTGCCCAGTGCCCAGGGGTGCCCAGTAGTTGCCCAGTGCCCAGCTGCCCAGCTGCCCAGTGCCCAGGACTGCCCAGGAGTTGTCCCCTACTATTCCTATCTTGCCCAGTCTGCCCAGGCTCGCTCGCGTGCCCAGCAGAAATTCGTGCCCAGCCTGCCCAGGGGCCTCGCGACTTTTGCCCAGTGCCTGCCCAG"
    motif = "TGCCCAGTG"
    found = find_motifs_as_indices(genome, motif)
    result = [motif for motif in found]
    pretty_result = " ".join([str(w) for w in result])
    print(pretty_result)


def generate_matrix(strings):
    matrix = []
    for i in strings:
        matrix.append([c for c in i])
    transposed_matrix = list(zip(*matrix))

    return transposed_matrix


def find_most_common_ancestor(filename):
    data = parse_fasta_file(filename)
    dna_strings = [dna_string for _, dna_string in data.items()]
    matrix = generate_matrix(dna_strings)

    column_counts = dict([("A", []), ("C", []), ("G", []), ("T", [])])
    consensus_string = ""

    for row in matrix:
        nucleotide_counter = Counter(row)
        most_common_nucleotide = nucleotide_counter.most_common(1)[0][0]
        consensus_string += most_common_nucleotide
        column_counts['A'].append(nucleotide_counter['A'])
        column_counts['C'].append(nucleotide_counter['C'])
        column_counts['G'].append(nucleotide_counter['G'])
        column_counts['T'].append(nucleotide_counter['T'])

    return consensus_string, column_counts


def rosalind_common_ancestor(filename, output_file):
    consensus_string, profile = find_most_common_ancestor(filename)
    f = open(output_file, "w+")
    f.write(consensus_string + "\n")
    print(consensus_string)

    for i, v in profile.items():
        numbers_as_string = " ".join([str(c) for c in v])
        pretty ="{}: {}\n".format(i, numbers_as_string)
        f.write(pretty)
        print(pretty)

    f.close()


class Node:
    def __init__(self, key, data):
        self.prefix = data[:3]
        self.postfix = data[-3:]
        self.data = data
        self.neighbours = []
        self.key = key

    def me(self):
        print(self.key, self.prefix, self.postfix, self.data, self.neighbours)

    def add_neighbour(self, key):
        self.neighbours.append(key)


def populate_graph_for_adjacency_list(data):
    graph = []
    for key, data in data.items():
        nod = Node(key, data)
        graph.append(nod)

    return graph


def get_adjacency_list_for_collection(a_collection):
    for g in a_collection:
        for gg in a_collection:
            if g.key != gg.key:
                if g.postfix == gg.prefix:
                    g.add_neighbour(gg.key)

    return a_collection


def ros_overlap_graph():
    data = parse_fasta_file("rosalind_grph.txt")
    # data = parse_fasta_file("rosalind_overlap_graph.txt")
    graph = populate_graph_for_adjacency_list(data)
    graph = get_adjacency_list_for_collection(graph)

    for g in graph:
        for i in g.neighbours:
            print(g.key, i)

from prob import punnett_square_probability


def ros_calculating_expected_offspring():
    number_of_couples = [1, 0, 0, 1, 0, 1]
    number_of_couples = [18209, 16806, 17553, 19481, 19333, 18689]
    offspring = 2
    couples_genotypes = ["AA-AA", "AA-Aa", "AA-aa", "Aa-Aa", "Aa-aa", "aa-aa"]
    couples_pairs = [(*couple.split("-"),) for couple in couples_genotypes]

    couples = list(zip(couples_pairs, number_of_couples))
    offsprings_with_allele = 0
    for couple in couples:
        probability_of_allele = punnett_square_probability(*couple[0], "A")
        offsprings_with_allele += probability_of_allele * couple[1] * offspring

    print(offsprings_with_allele)


def get_common_substring(data, size):
    """Get the common substring given a collection of words and
    a substring size """
    sets = []
    for word in data:
        _frames = []
        for i in range(len(word) - size + 1):
            _frames.append(word[i:i+size])
        sets.append(set(_frames))
    return set.intersection(*sets)


def get_longest_common_substring(data, length, minimum):
    """Get the longest common substring starting
    with the the length of the smallest word in the collection"""
    for i in reversed(range(minimum, length + 1)):
        result = get_common_substring(data, i)
        if result:
            answer = result.pop()
            print(f"{answer} {len(answer)}")
            break


def ros_longest_substring():
    fasta_data = parse_fasta_file("rosalind_shared_motif.txt")
    fasta_data = parse_fasta_file("rosalind_lcsm.txt")
    data = [value for _, value in fasta_data.items()]
    length = len(min(data))
    minimum = 1
    get_longest_common_substring(data, length, minimum)


def ros_independent_alleles():
    num_of_generations = 5
    offspring = 2
    num_organisms = 8
    population = offspring ** num_of_generations
    punnet_probability_AaBb = 0.25 #Base on combination of offspring with alleles AaBb
    punnet_probability_of_NO_AaBB = 1 - punnet_probability_AaBb

    probability = 0
    # Binomial distribution
    for generation in range(num_organisms, population + 1):
        probability_current_generation = (factorial(population) / (factorial(generation) * factorial(population - generation)))  * (punnet_probability_AaBb**generation) * (punnet_probability_of_NO_AaBB**(population - generation))
        probability += probability_current_generation

    print(f"{probability:.3}")


def check_motif(word):
    if not word.startswith("N"):
        return False
    if word[1] == "P":
        return False
    if not (word[2] == "S" or word[2] == "T"):
        return False
    if word[1] == "P":
        return False

    return True


def finding_a_protein_motif(stack):
    # N{P}[ST]{P}
    needle_length = 4
    found_positions = []
    for i in range(len(stack) - needle_length + 1):
        substring = stack[i:i+needle_length]
        if check_motif(substring):
            found_positions.append(i)
    results = [index + 1 for index in found_positions]

    return results


def read_file_protein(filename):
    with open("./motif/"+filename) as fasta_file:
        protein_id = ""
        data = ""
        for line in fasta_file.readlines():
            if line.startswith(">"):
                protein_id = line.split("|")[1]
            else:
                data += line.strip()
        result = finding_a_protein_motif(data)

        return protein_id, result
import os
import requests


def download(url: str, dest_folder: str):
    if not os.path.exists(dest_folder):
        os.makedirs(dest_folder)  # create folder if it does not exist

    filename = url.split('/')[-1].replace(" ", "_")  # be careful with file names
    file_path = os.path.join(dest_folder, filename)

    r = requests.get(url, stream=True)
    if r.ok:
        # print("saving to", os.path.abspath(file_path))
        with open(file_path, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024 * 8):
                if chunk:
                    f.write(chunk)
                    f.flush()
                    os.fsync(f.fileno())
    else:  # HTTP status code 4XX/5XX
        print("Download failed: status code {}\n{}".format(r.status_code, r.text))


def create_uniprot_url(id):
    base = "http://www.uniprot.org/uniprot/"
    file_extension = ".fasta"
    filename = id + file_extension
    url = base + filename

    return url, filename


def ros_finding_a_protein_motif(filename):
    with open(filename) as file:
        protein_ids = [line.strip() for line in file.readlines()]

    for id in protein_ids:
        url, file_name = create_uniprot_url(id)
        download(url, "./motif")
        uniprot_id, results = read_file_protein(file_name)
        if results:
            print(id)
            print(" ".join([str(w) for w in results]))
# Inferring mRNA from Protein
def ros_inferring_mrna_from_protein(file_path):
    with open(file_path, 'r') as file:
        protein_string = file.read()
        protein_string = protein_string.replace('\n', '')
    # protein_string = "MA"
    from bio_structs import CODONS_PER_AMINOACID
    modulus = 1000000

    # Calculate the total number of RNA strings
    total_aminoacids = 1
    for amino_acid in protein_string:
        total_aminoacids = (total_aminoacids * CODONS_PER_AMINOACID[amino_acid])
    total_rna_strings = total_aminoacids * CODONS_PER_AMINOACID['_']
    total_rna_strings = total_rna_strings % modulus
    print(f"The total number of different RNA strings is: {total_rna_strings}")


# ORF
def convert_to_rna_frames(frames):
    rna_frames = []
    for i, frame in enumerate(frames):
        rn_frame = [f.replace("T", "U") for f in frame]
        rna_frames.append(rn_frame)

    return rna_frames
def find_sections(lst, start_char, end_char):
    sections = []
    start_indices = [i for i, x in enumerate(lst) if x == start_char]
    end_indices = [i for i, x in enumerate(lst) if x == end_char]

    for start_idx in start_indices:
        for end_idx in end_indices:
            if end_idx > start_idx:
                section = ''.join(lst[start_idx:end_idx])
                sections.append(section)
                break

    return sections

def ros_open_reading_frames(file_path_dna_sequence: str):
    fasta = parse_fasta_file(file_path_dna_sequence)
    dna_sequence = [value for value in fasta.values()][0]

    # dna_sequence = "AGCCATGTAGCTAACTCAGGTTACATGGGGATGACCCCGCGACTTGGATTAGAGTCTCTTTTGGAATAAGCCTGAATGATCCGAGTAGCATCTCAG"
    myseq = BioSeq(dna_sequence)
    frames = myseq.gen_reading_frames()
    candidate_protein_strings = []
    for frame in frames:
        found = find_sections(frame, 'M', '_')
        if found:
            candidate_protein_strings.extend(found)
    result = [candidate for candidate in set(candidate_protein_strings)]
    print(result)

# ros_counting_nucleotides()
# ros_transcribing_dna_to_rna()
# ros_complementing_a_Strand_of_DNA()
# ros_rabbits()
# ros_get_highest_gc_content("rosalind_gc.txt")
# ros_hamming_distance()
# translating_rna_into_protein("rosalind_prot.txt", "rosalind_prot_answer.txt")
# rosalind_finding_a_motif_in_dna()
# rosalind_common_ancestor("rosalind_cons.txt", "rosalind_consensus_answer.txt")
# print(ros_rabbits_mortal_calculation(87,20))
# ros_overlap_graph()
# ros_calculating_expected_offspring()
# ros_longest_substring()
# ros_independent_alleles()
# ros_finding_a_protein_motif("rosalind_mprt.txt")

ros_inferring_mrna_from_protein(ROOT_DIR / "rosalind_mrna.txt")
# ros_open_reading_frames("rosalind_orf.txt")