DNA_Nucleotides = ["A", "C", "G", "T"]
RNA_Nucleotides = ["A", "C", "G", "U"]
DNA_Codons = {
    # 'M' - START, '_' - STOP
    "GCT": "A", "GCC": "A", "GCA": "A", "GCG": "A",
    "TGT": "C", "TGC": "C",
    "GAT": "D", "GAC": "D",
    "GAA": "E", "GAG": "E",
    "TTT": "F", "TTC": "F",
    "GGT": "G", "GGC": "G", "GGA": "G", "GGG": "G",
    "CAT": "H", "CAC": "H",
    "ATA": "I", "ATT": "I", "ATC": "I",
    "AAA": "K", "AAG": "K",
    "TTA": "L", "TTG": "L", "CTT": "L", "CTC": "L", "CTA": "L", "CTG": "L",
    "ATG": "M",
    "AAT": "N", "AAC": "N",
    "CCT": "P", "CCC": "P", "CCA": "P", "CCG": "P",
    "CAA": "Q", "CAG": "Q",
    "CGT": "R", "CGC": "R", "CGA": "R", "CGG": "R", "AGA": "R", "AGG": "R",
    "TCT": "S", "TCC": "S", "TCA": "S", "TCG": "S", "AGT": "S", "AGC": "S",
    "ACT": "T", "ACC": "T", "ACA": "T", "ACG": "T",
    "GTT": "V", "GTC": "V", "GTA": "V", "GTG": "V",
    "TGG": "W",
    "TAT": "Y", "TAC": "Y",
    "TAA": "_", "TAG": "_", "TGA": "_"
}
RNA_Codons = {
    # 'M' - START, '_' - STOP
    "GCU": "A", "GCC": "A", "GCA": "A", "GCG": "A",
    "UGU": "C", "UGC": "C",
    "GAU": "D", "GAC": "D",
    "GAA": "E", "GAG": "E",
    "UUU": "F", "UUC": "F",
    "GGU": "G", "GGC": "G", "GGA": "G", "GGG": "G",
    "CAU": "H", "CAC": "H",
    "AUA": "I", "AUU": "I", "AUC": "I",
    "AAA": "K", "AAG": "K",
    "UUA": "L", "UUG": "L", "CUU": "L", "CUC": "L", "CUA": "L", "CUG": "L",
    "AUG": "M",
    "AAU": "N", "AAC": "N",
    "CCU": "P", "CCC": "P", "CCA": "P", "CCG": "P",
    "CAA": "Q", "CAG": "Q",
    "CGU": "R", "CGC": "R", "CGA": "R", "CGG": "R", "AGA": "R", "AGG": "R",
    "UCU": "S", "UCC": "S", "UCA": "S", "UCG": "S", "AGU": "S", "AGC": "S",
    "ACU": "T", "ACC": "T", "ACA": "T", "ACG": "T",
    "GUU": "V", "GUC": "V", "GUA": "V", "GUG": "V",
    "UGG": "W",
    "UAU": "Y", "UAC": "Y",
    "UAA": "_", "UAG": "_", "UGA": "_"
}

STOP_CODONS = [codon for codon, value in RNA_Codons.items() if value == '_']
START_CODONS = [codon for codon, value in RNA_Codons.items() if value == 'M']
# Define the codon count for each amino acid
CODONS_PER_AMINOACID = {
    'A': 4, 'C': 2, 'D': 2, 'E': 2, 'F': 2,
    'G': 4, 'H': 2, 'I': 3, 'K': 2, 'L': 6,
    'M': 1, 'N': 2, 'P': 4, 'Q': 2, 'R': 6,
    'S': 6, 'T': 4, 'V': 4, 'W': 1, 'Y': 2,
    '_': 3  # Stop codons
}


MONOISOTOPIC_MASS_TABLE = {
    'A': 71.03711,  # Alanine
    'C': 103.00919, # Cysteine
    'D': 115.02694, # Aspartic acid
    'E': 129.04259, # Glutamic acid
    'F': 147.06841, # Phenylalanine
    'G': 57.02146,  # Glycine
    'H': 137.05891, # Histidine
    'I': 113.08406, # Isoleucine
    'K': 128.09496, # Lysine
    'L': 113.08406, # Leucine
    'M': 131.04049, # Methionine
    'N': 114.04293, # Asparagine
    'P': 97.05276,  # Proline
    'Q': 128.05858, # Glutamine
    'R': 156.10111, # Arginine
    'S': 87.03203,  # Serine
    'T': 101.04768, # Threonine
    'V': 99.06841,  # Valine
    'W': 186.07931, # Tryptophan
    'Y': 163.06333  # Tyrosine
}

# Rosalind uses the term "genetic string" to refer to any string modeling a biological polymer.
# The term "sequence" is also frequently used throughout the biological world. We will consider three types of genetic strings:
#
# DNA strings: formed over the alphabet {A, C, G, T} to model single strands of DNA
#     (the opposing strand can be inferred by taking the string's reverse complement);
# RNA strings: formed over the alphabet {A, C, G, U} to model strands of RNA;
# protein strings: formed over a 20-symbol alphabet containing all the letters of the English alphabet except for B, J, O, U, X, and Z.
#     Used to model the polypeptide chains that make up much larger protein structures.