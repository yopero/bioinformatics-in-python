import unittest
from DNAToolkit import proteins_from_rf 

class TestProteinsFromORF(unittest.TestCase):

    def test_idealORF(self):
        test_rf_frame = ['M', 'V', 'I', 'S', 'R', 'S', 'S', 'H', 'G', 'A', 'Y', 'V', 'S', 'I', 'K', '_']
        expected_list = ['M', 'V', 'I', 'S', 'R', 'S', 'S', 'H', 'G', 'A', 'Y', 'V', 'S', 'I', 'K']
        
        expected =      ["".join(expected_list)]
        self.assertEqual(expected, proteins_from_rf(test_rf_frame)) 

    def test_skew_right(self):
        test_rf_frame = ['V', 'V', 'I', 'S', 'R', 'S', 'M', 'H', 'G', 'A', 'Y', 'V', 'S', 'I', 'K', '_']
        expected_list = ['M', 'H', 'G', 'A', 'Y', 'V', 'S', 'I', 'K']
        
        expected =      ["".join(expected_list)]
        self.assertEqual(expected, proteins_from_rf(test_rf_frame)) 

    def test_skew_left(self):
        test_rf_frame = ['M', 'V', 'I', 'S', 'R', 'S', 'R', 'H', '_', 'A', 'Y', 'V', 'S', 'I', 'K', 'K']
        expected_list = ['M', 'V', 'I', 'S', 'R', 'S', 'R', 'H']
        
        expected =      ["".join(expected_list)]
        self.assertEqual(expected, proteins_from_rf(test_rf_frame)) 

    def test_middle(self):
        test_rf_frame = ['M', 'V', 'I', 'S', 'R', 'S', 'R', 'H', '_', 'A', 'Y', 'V', 'S', 'I', 'K', 'K']
        expected_list = ['M', 'V', 'I', 'S', 'R', 'S', 'R', 'H']
        
        expected =      ["".join(expected_list)]
        self.assertEqual(expected, proteins_from_rf(test_rf_frame)) 

    def test_two_frames(self):
        test_rf_frame = ['M', 'V', 'I', 'S', 'R', 'S', 'R', 'H', 'M', 'A', 'Y', 'V', 'S', 'I', 'K', '_']
        list_a = ['M', 'V', 'I', 'S', 'R', 'S', 'R', 'H', 'M', 'A', 'Y', 'V', 'S', 'I', 'K']
        list_b = ['M', 'A', 'Y', 'V', 'S', 'I', 'K']
        
        expected =      ["".join(list_a)]
        expected +=      ["".join(list_b)]
        self.assertEqual(expected, proteins_from_rf(test_rf_frame)) 

