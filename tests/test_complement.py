import unittest
from DNAToolkit import get_complement

class TestComplement(unittest.TestCase):

    def test_reverse_A(self):
        seq = "AAAA"
        self.assertEqual("TTTT", get_complement(seq))

    def test_reverse_T(self):
        seq = "TTT"
        self.assertEqual("AAA", get_complement(seq))

    def test_reverse_G(self):
        seq = "GGG"
        self.assertEqual("CCC", get_complement(seq))

    def test_reverse_C(self):
        seq = "CCCC"
        self.assertEqual("GGGG", get_complement(seq))

    def test_mixed(self):
        seq = "ACGT"
        self.assertEqual("TGCA", get_complement(seq))
