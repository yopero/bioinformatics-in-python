import unittest
from DNAToolkit import gc_content
class TestGCcontent(unittest.TestCase):

    def test_fifty(self):
        seq = "GCAA"
        self.assertEqual(50, gc_content(seq))

    def test_twenty_five(self):
        seq = "GTAA"
        self.assertEqual(25, gc_content(seq))

    def test_seventy_five(self):
        seq = "AGCG"
        self.assertEqual(75, gc_content(seq))

    def test_cero(self):
        seq = "ATATAT"
        self.assertEqual(0, gc_content(seq))

    def test_ten(self):
        seq = "GTATATAAAT"
        self.assertEqual(10, gc_content(seq))

    def test_hundred(self):
        seq = "GCGCGCGCGCG"
        self.assertEqual(100, gc_content(seq))
