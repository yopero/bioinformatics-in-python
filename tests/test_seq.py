import unittest
from DNAToolkit import validateSeq

class TestValidation(unittest.TestCase):

    def test_good_all_upper(self):
        seq = "ACGT"
        self.assertEqual(seq, validateSeq(seq))

    def test_good_all_lower(self):
        seq = "acgt"
        expected = "ACGT"
        self.assertEqual(expected, validateSeq(seq))
    
    def test_good_mixed(self):
        seq = "AcGt"
        expected = "ACGT"
        self.assertEqual(expected, validateSeq(seq))
    
    def test_bad_all_upper(self):
        seq = "APGT"
        self.assertEqual(False, validateSeq(seq))

    def test_bad_all_lower(self):
        seq = "apgt"
        self.assertEqual(False, validateSeq(seq))
    
    def test_bad_mixed(self):
        seq = "ApGx"
        self.assertEqual(False, validateSeq(seq))

    def test_bad_empty(self):
        seq = ""
        self.assertEqual(False, validateSeq(seq))