import unittest
from DNAToolkit import validateSeq, generate_random_seq

class TestRandSeq(unittest.TestCase):

    def test_size_default(self):
        seq = generate_random_seq()
        self.assertEqual(20, len(seq))

    def test_given_size(self):
        size = 30
        seq = generate_random_seq(size)
        self.assertEqual(size, len(seq))
