import unittest
from DNAToolkit import validateSeq, generate_random_seq, countNucFrequency

class TestCountFrequency(unittest.TestCase):
    def setUp(self):
        self.empty_conter = {'A': 0, 'C': 0, 'G': 0, 'T': 0}
    def test_empty(self):
        seq = ""
        self.assertEqual(self.empty_conter, countNucFrequency(seq))

    def test_all_of_one(self):
        seq = "AAAAAA"
        self.empty_conter["A"] = len(seq)
        self.assertEqual(self.empty_conter, countNucFrequency(seq))
    
    def test_mixed(self):
        seq = "ACGTACGT"
        self.empty_conter["A"] = 2
        self.empty_conter["C"] = 2
        self.empty_conter["G"] = 2
        self.empty_conter["T"] = 2
        self.assertEqual(self.empty_conter, countNucFrequency(seq))