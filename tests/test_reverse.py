import unittest
from DNAToolkit import reverse_complement

class TestReverse(unittest.TestCase):

    def test_reverse_one(self):
        seq = "A"
        self.assertEqual("T", reverse_complement(seq))

    def test_reverse_same_odd(self):
        seq = "TTT"
        self.assertEqual("AAA", reverse_complement(seq))

    def test_reverse_same_even(self):
        seq = "GGGG"
        self.assertEqual("CCCC", reverse_complement(seq))

    def test_reverse_end_middle_end(self):
        seq = "CTC"
        self.assertEqual("GAG", reverse_complement(seq))

    def test_reverse_end_middle_end_B(self):
        seq = "CCTCC"
        self.assertEqual("GGAGG", reverse_complement(seq))

    def test_reverse_half_half(self):
        seq = "AAAGGG"
        self.assertEqual("CCCTTT", reverse_complement(seq))

    def test_reverse_flanked(self):
        seq = "AAACAAA"
        self.assertEqual("TTTGTTT", reverse_complement(seq))

    def test_reverse_mirror(self):
        seq = "ACGT"
        self.assertEqual("ACGT", reverse_complement(seq))

    def test_reverse_mixed(self):
        seq = "ACGTGTT"
        self.assertEqual("AACACGT", reverse_complement(seq))
    
    