import itertools
from pathlib import Path

def get_kmers(path_file):
    with open(path_file, "r") as f:
        lines = f.readlines()
        symbols = lines[0].strip().split(" ")
        number = int(lines[1].strip())

    all_strings_raw = itertools.product(symbols, repeat=number)
    all_strings = ["".join(string_) for string_ in all_strings_raw]
    return all_strings




current_dir = Path(__file__).parent
# TEST
test_file = current_dir / "test_input.txt"

kmers = get_kmers(test_file)
expected = [
    "AA", "AC", "AG", "AT",
    "CA", "CC", "CG", "CT",
    "GA", "GC", "GG", "GT",
    "TA", "TC", "TG", "TT"
]
assert expected == kmers

with open(current_dir / "test_output.txt", "w") as f:
    for kmer in kmers:
        print(kmer, file=f)


# ACTUAL DATA
test_file = current_dir / "input_enumerating_kmers_lexicographically.txt"

kmers = get_kmers(test_file)

with open(current_dir / "23_output.txt", "w") as f:
    for kmer in kmers:
        print(kmer, file=f)

