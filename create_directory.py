import pathlib
from utilities import ROOT_DIR
import argparse

def create_sub_directory(dir_name: str, extension: str) -> None:
    try:
        file_name = dir_name.split("-")[1].replace(" ", "_").lower()
        file_names = [f"{file_name}.py", f"test_input.{extension}", f"input_{file_name}.{extension}", "__init__.py"]
        dir_path = ROOT_DIR / dir_name

        pathlib.Path(dir_path).mkdir(exist_ok=False)

        for file_name in file_names:
            pathlib.Path( dir_path /file_name).touch()

    except FileExistsError:
        print("Directory already exists")

    print(f"Skeleton created at '{dir_name}'")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dir-name')
    parser.add_argument('-e', '--file-extension', default="txt")
    args = parser.parse_args()

    create_sub_directory(args.dir_name, args.file_extension)
