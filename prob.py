from itertools import permutations, combinations


def punnett_square_probability(mum, dad, allele_to_check):
    results = []
    for mum_allele in mum:
        for dad_allele in dad:
            alleles = mum_allele + dad_allele
            if allele_to_check in alleles:
                results.append(1)
            else:
                results.append(0)
    found_allels_count = sum(results)
    total_population_count = len(results)
    probability_of_allel = found_allels_count / total_population_count
    
    return probability_of_allel


def get_population(types_data):
    _population = []
    for organism_type in types_data:
        for one_organism in range(organism_type['number']):
            _population.append(organism_type["symbol"])
    
    return _population


def probability_of_pair(mum, dad, types_data):
    if mum == dad:
        _mum = next(item for item in types_data if item["symbol"] == mum)
        _dad = next(item for item in types_data if item["symbol"] == dad)
        _number_of_type = _mum['number']
        _pair_probability = (_number_of_type / TOTAL_POPULATION)*((_number_of_type - 1) / (TOTAL_POPULATION - 1))

        return _pair_probability

    _mum = next(item for item in types_data if item["symbol"] == mum)
    _dad = next(item for item in types_data if item["symbol"] == dad)
    _number_of_type_mum = _mum['number']
    _number_of_type_dad = _dad['number']
    _pair_probability =(_number_of_type_mum / TOTAL_POPULATION) * (_number_of_type_dad / (TOTAL_POPULATION - 1))
    
    return _pair_probability


def get_types_data(count_homo_dominant, count_hetero, count_homo_recessive):
    _types_data = [
        {"name": "homozygous_dominant", "number": count_homo_dominant, "symbol": "AA"},
        {"name": "heterozygous", "number": count_hetero, "symbol": "Aa"},
        {"name": "homozygous_recessive", "number": count_homo_recessive, "symbol": "aa"},
    ]

    return _types_data, count_homo_dominant + count_hetero + count_homo_recessive


def get_probability_of_allele_in_population(types_data, possible_combinations, allele_to_check):
    result = 0
    for pair_combination in possible_combinations:
        allele_probability = punnett_square_probability(*pair_combination, allele_to_check)
        pair_probability = probability_of_pair(*pair_combination, types_data)
        probability = allele_probability * pair_probability
        result += probability

    return result


k = 20
m = 23
n = 30

types_data, TOTAL_POPULATION = get_types_data(k, m, n)
population = get_population(types_data)
parents_combinations = set(permutations(population, 2))
prob = get_probability_of_allele_in_population(types_data, parents_combinations, "A")
# print(prob)
