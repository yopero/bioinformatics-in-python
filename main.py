from bio_seq import BioSeq

test_dna = BioSeq()
test_dna.generate_random_seq(40, "DNA")

print(test_dna.get_seq_info())
print(test_dna.nucleotide_frequency())
print(test_dna.transcription())
print(test_dna.reverse_complement())
print(test_dna.gc_content())
print(test_dna.gc_content_subsec())
print(test_dna.transcription())
print(test_dna.codon_usage("L"))
for orf in test_dna.gen_reading_frames():
    print(orf)

print(test_dna.proteins_from_rf(['L', 'Y', 'M', 'P', 'N', 'A', 'W', '_', 'D', 'D', 'K', 'G', 'Q']))
print(test_dna.all_proteins_from_orfs())