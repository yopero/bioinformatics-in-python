from unittest import expectedFailure

from bio_seq import BioSeq
from rosalind import  parse_fasta_file
from pathlib import Path


def delete_introns(file_path: Path) -> str:
    parsed_fasta = parse_fasta_file(file_path)
    fasta_entries = list(parsed_fasta.values())

    dna_string, introns = fasta_entries[0], fasta_entries[1:]
    initial_length = len(dna_string)
    for intron in introns:
        tmp = dna_string
        dna_string = dna_string.replace(intron, "")
        if dna_string == tmp:
            print(f"{intron} not found")
        else:
            print(f"{intron} found")

    # deletion test
    introns_count = sum([len(a) for a in introns])
    assert initial_length - len(dna_string) == introns_count

    return dna_string


def get_protein_string(file_path: Path) -> str:
    dna_string = delete_introns(file_path)

    seq = BioSeq(dna_string)
    aminoacids_list = seq.translate_seq()
    aminoacids_string = "".join(aminoacids_list)
    aminoacids = aminoacids_string.replace("_", "")

    return aminoacids

# TEST
current_dir = Path(__file__).parent
test_file = current_dir / "test_input.fasta"
prot_string = get_protein_string(test_file)
expected = "MVYIADKQHVASREAYGHMFKVCA"
assert expected == prot_string

# ACTUAL DATA
current_dir = Path(__file__).parent
data_file = current_dir / "input_rna_splicing.fasta"
prot_string = get_protein_string(data_file)
with open(current_dir / "22_output.txt", "w") as file:
    print(prot_string, file=file)
