import os
from itertools import permutations

up_to = 5


current_dir = os.path.dirname(__file__)

result = list(permutations(range(1, up_to + 1)))

with open(f"{current_dir}/19-output.txt", "w") as file:
    print(len(result), file=file)
    for item in result:
        for elem in item:
            print(elem, end=" ", file=file)
        print(file=file)
