import operator
from pathlib import Path
from enum import IntEnum

# class SubSequenceType(IntEnum):
#     ASCENDING = 1
#     DESCENDING = 2
#
# def get_good_seq(aseq, value_to_compare, comparator, subsequences_found=None):
#     print("ai")
#     # Adding the min, or max as first element
#     if subsequences_found is None:
#         subsequences_found = []
#     subsequence = [value_to_compare]
#     # keep track of seen numbers
#     sub_index = []
#     compare_value_ = value_to_compare
#     for i, element in enumerate(aseq):
#         # if element < compare_value_:
#         if comparator(element, compare_value_):
#             subsequence.append(element)
#             compare_value_ = element
#             sub_index.append(i)
#             # no elements left in the sublist
#             if i == len(aseq) - 1:
#                 subsequences_found.append(subsequence)
#                 return aseq, subsequence, comparator, subsequences_found
#         else:
#             # chain of ascending or descending is broken here in this branch
#             if sub_index:
#                 for _ in sub_index:
#                     aseq.pop(0)
#             else:
#                 aseq.pop(0)
#             subsequences_found.append(subsequence)
#             get_good_seq(aseq, value_to_compare, comparator, subsequences_found)
#     found = subsequences_found[:]
#     subsequences_found = []
#     return found
#
# def get_subsequence(permutation: list, order:SubSequenceType) -> list:
#     if order == SubSequenceType.ASCENDING:
#         compare_value = min(permutation)
#         comparator = operator.gt
#     if order == SubSequenceType.DESCENDING:
#         compare_value = max(permutation)
#         comparator = operator.lt
#     compare_index = None
#
#     for index, element in enumerate(permutation):
#         if element == compare_value:
#             compare_index = index
#             break
#
#     sequence = permutation[compare_index:]
#     # sequence = [1,4,5,5,3,4,2,3]
#     right_side = sequence[1:]
#
#     tot = get_good_seq(right_side, compare_value, comparator)
#     # return the longest chain
#     return max(tot, key=len)


def get_subsequences(path_file):
    with open(path_file, "r") as f:
        lines = f.readlines()
        length = int(lines[0].strip())
        permutation = lines[1].strip().split(" ")
        permutation = [int(number) for number in permutation]

    # increasing_subsequences = get_subsequence(permutation, SubSequenceType.ASCENDING)
    # decreasing_subsequences = get_subsequence(permutation, SubSequenceType.DESCENDING)
    increasing_subsequences = longest_increasing_subsequence(permutation)
    decreasing_subsequences = longest_decreasing_subsequence(permutation)

    return {
        "increasing_subsequences": increasing_subsequences,
        "decreasing_subsequences": decreasing_subsequences
    }


def save_to_disk(file_path: Path, subsequences_found):
    with open(file_path, "w") as f:
        increasing_subsequences_str = [str(a) for a in subsequences_found["increasing_subsequences"]]
        print(" ".join(increasing_subsequences_str), file=f)
        decreasing_subsequences_str = [str(a) for a in subsequences_found["decreasing_subsequences"]]
        print(" ".join(decreasing_subsequences_str), file=f, end="")


def longest_decreasing_subsequence(seq):
    n = len(seq)
    LDS = [1] * n
    prevLDS = [-1] * n

    for i in range(1, n):
        for j in range(0, i):
            if seq[i] < seq[j] and LDS[i] < LDS[j] + 1:
                LDS[i] = LDS[j] + 1
                prevLDS[i] = j

    max_index = 0
    for i in range(1, n):
        if LDS[i] > LDS[max_index]:
            max_index = i

    lds = []
    current = max_index
    while current != -1:
        lds.append(seq[current])
        current = prevLDS[current]

    lds.reverse()
    return lds

def longest_increasing_subsequence(seq):
    n = len(seq)
    LIS = [1] * n
    prevLIS = [-1] * n

    for i in range(1, n):
        for j in range(0, i):
            if seq[i] > seq[j] and LIS[i] < LIS[j] + 1:
                LIS[i] = LIS[j] + 1
                prevLIS[i] = j

    max_index = 0
    for i in range(1, n):
        if LIS[i] > LIS[max_index]:
            max_index = i

    lis = []
    current = max_index
    while current != -1:
        lis.append(seq[current])
        current = prevLIS[current]

    lis.reverse()
    return lis

if "__main__" == __name__:

    # NOTE TO SELF: Object Oriented approach did not work as it was exisiding the max recursion limit
    # when dynamic programing forget about OOP
    current_dir = Path(__file__).parent
    # TEST
    test_file = current_dir / "test_input.txt"
    test_subsequences_found = get_subsequences(test_file)
    save_to_disk(current_dir / "test_output", test_subsequences_found)
    # ACTUAL DATA
    data_file = current_dir / "input_longest_increasing_subsequence.txt"
    test_subsequences_found = get_subsequences(data_file)
    save_to_disk(current_dir / "24_output", test_subsequences_found)

