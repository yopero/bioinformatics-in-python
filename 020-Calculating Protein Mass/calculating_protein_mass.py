import os
from bio_structs import MONOISOTOPIC_MASS_TABLE
current_dir = os.path.dirname(__file__)



def calculating_protein_mass(protein_string, rounded_to=3):
    total = 0
    for element in protein_string:
        total += MONOISOTOPIC_MASS_TABLE[element]

    return round(total, rounded_to)

# TEST
test_string = "SKADYEK"
expected = 821.392
assert calculating_protein_mass(test_string) == expected
print("ok")

# ACTUAL DATA
with open(f"{current_dir}/rosalind_prtm.txt", 'r') as file:
    input_protein_string = file.read().strip()

print(calculating_protein_mass(input_protein_string))