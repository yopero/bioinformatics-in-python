from bio_structs import DNA_Nucleotides, DNA_Codons, RNA_Nucleotides, RNA_Codons
from collections import Counter
import random


def gc_count(seq):
    count = ((seq.count("C") + (seq.count("G"))) / len(seq) * 100.0)
    return float("{:.6f}".format(count))
    
class BioSeq:
    """DNA sequence class. Default values ACGT, DNA, no level"""

    def __init__(self, seq="ATCG", seq_type="DNA", label="No Label"):
        """Sequence initialization, validation"""
        self.seq = seq.upper()
        self.label = label
        self.seq_type = seq_type
        self.is_valid = self.__validate()
        assert self.is_valid, "Provided data does not seem to be correct {}".format(self.seq)

    def __validate(self):
        """Validates the sequence accepting only the 4 Bases ACGT"""
        if self.seq_type == "DNA":
            return set(DNA_Nucleotides).issuperset(self.seq)
        if self.seq_type == "RNA":
            return set(RNA_Nucleotides).issuperset(self.seq)

    def get_seq_info(self):
        """Returns 4 strngs. Full sequence information"""
        return (
            f"[Label]:    {self.label}\n"
            f"[Sequence]: {self.seq}\n"
            f"[Biotype]:  {self.seq_type}\n"
            f"[Lenght]:   {len(self.seq)}\n"
        )

    def get_seq_biotype(self):
        return self.seq_type

    def generate_random_seq(self, lenght=10, seq_type="DNA"):
        """Generates a random dna seq of a given size"""
        rand_seq = "".join([random.choice(DNA_Nucleotides) for nuc in range(lenght)])

        self.__init__(rand_seq, seq_type, "Randomly generated sequence")

    def nucleotide_frequency(self, numbers_only=False, as_counter=False):
        """Count nucleotides in a given sequence. Return dictionary"""
        counter = Counter(self.seq)
        if numbers_only:
            ordered = sorted(counter.items())
            numbers = [str(a[1]) for a in ordered]

            return " ".join(numbers)

        if as_counter:
            return counter

        return dict(Counter(self.seq))

    def transcription(self):
        """DNA->RNA Transcription. Replaces Thymine with Uracil"""
        return self.seq.replace("T", "U")

    def get_complement(self, seq):
        """Gets the complement from str """
        mapping = str.maketrans("ATCG", "TAGC")
        seq_complement = seq.translate(mapping)

        return seq_complement

    def reverse_complement(self):
        """Reverses the complement of string"""
        return self.get_complement(self.seq)[::-1]

    def gc_content(self):
        """GC Content in a DNA/RNA sequence"""
        # return round((self.seq.count("C") + (self.seq.count("G"))) / len(self.seq) * 100)
        return gc_count(self.seq)

    def gc_content_subsec(self, k=20):
        res = []
        for i in range(0, len(self.seq) - k + 1, k):
            subseq = self.seq[i:i + k]
            res.append(gc_count(subseq))

        return res

    def translate_seq(self, init_pos=0):
        """Translates a DNA sequence into aminoacid sequence"""
        if self.seq_type == "DNA":
            return [DNA_Codons[self.seq[pos:pos + 3]] for pos in range(init_pos, len(self.seq) - 2, 3)]
        if self.seq_type == "RNA":
            return [RNA_Codons[self.seq[pos:pos + 3]] for pos in range(init_pos, len(self.seq) - 2, 3)]

    def codon_usage(self, aminoacid):
        """Provides the frequency into an aminoacid sequence"""
        tmpList = []
        for i in range(0, len(self.seq) - 2, 3):
            if DNA_Codons[self.seq[i:i + 3]] == aminoacid:
                tmpList.append(self.seq[i:i + 3])

        freqDict = (dict(Counter(tmpList)))
        totalWeight = sum(freqDict.values())
        for seq in freqDict:
            freqDict[seq] = round(freqDict[seq] / totalWeight, 2)

        return freqDict

    def gen_reading_frames(self):
        frames = []
        frames.append(self.translate_seq(0))
        frames.append(self.translate_seq(1))
        frames.append(self.translate_seq(2))
        tmp_seq = BioSeq(self.reverse_complement(), self.seq_type)
        frames.append(tmp_seq.translate_seq(0))
        frames.append(tmp_seq.translate_seq(1))
        frames.append(tmp_seq.translate_seq(2))
        del tmp_seq

        return frames

    def proteins_from_rf(self, aa_seq):
        """Compute all possible proteins in an aminoacids sequence
        and return a list of all possible proteins"""
        current_protein = []
        proteins = []
        for aa in aa_seq:
            if aa == "_":
                # STOP accumulating aminoacids if _ - STOP was found
                if current_protein:
                    for p in current_protein:
                        proteins.append(p)
                    current_protein = []
            else:
                # START accumulating aminoacids if M - START found
                if aa == "M":
                    current_protein.append("")
                for i in range(len(current_protein)):
                    current_protein[i] += aa

        return proteins

    def all_proteins_from_orfs(self, startReadPos=0, endReadPos=0, ordered=False):
        """Compute all possible proteins for all open reading frames"""
        """Protein search DB: https://www.ncbi.nl.nih.gov/nuccore/NM_001185097.2"""
        """API can be use to pull protein info"""
        if endReadPos > startReadPos:
            tmp_seq = BioSeq(self.seq[startReadPos:endReadPos], self.seq_type)
            rfs = tmp_seq.gen_reading_frames()
        else:
            rfs = self.gen_reading_frames() 

        res = []

        for rf in rfs:
            prots = self.proteins_from_rf(rf)
            for p in prots:
                res.append(p)

            if ordered:
                return sorted(res, key=len, reverse=True)

            return res
