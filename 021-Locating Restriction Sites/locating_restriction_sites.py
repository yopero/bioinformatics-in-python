from pathlib import Path
from rosalind import parse_fasta_file

current_dir = Path(__file__).parent


def check_palindrome(word):
    matching ={
        'A': 'T',
        'T': 'A',
        'C': 'G',
        'G': 'C',
    }
    if word == "ATGCAT":
        pass
    reverse = word[::-1]
    complement_list = [matching[l] for l in reverse]
    complement_string = "".join(complement_list)

    return word == complement_string

def find_restriction_sites(seq):
    lg =  len(seq) + 1
    ind = 0
    index = 4
    palindromes = []
    while ind < lg - 3 :
        while index < lg:
            print(ind, index)
            word = seq[ind:index]
            if check_palindrome(word):
                palindromes.append((ind+1, len(word), word))
            index += 2
        ind = ind + 1
        index = 4 + ind

    return palindromes


test_fasta = parse_fasta_file(f"{current_dir}/test.fasta")
seq = [value for value in test_fasta.values()][0]
pals = find_restriction_sites(seq)
print(pals)
print(len(pals))
for item in pals:
    print(f"{item[0]} {item[1]}")


test_fasta = parse_fasta_file(f"{current_dir}/input.fasta")
seq = [value for value in test_fasta.values()][0]
pals = find_restriction_sites(seq)
print(pals)
print(len(pals))
with open(f"{current_dir}/21-output.txt", "w") as file:
    for item in pals:
        print(f"{item[0]} {item[1]}", file=file)