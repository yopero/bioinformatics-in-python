from pathlib import Path

ROOT_DIR = Path(__file__).parent

def colored(seq):
    bcolors = {
        'A': '\033[92m',
        'C': '\033[94m',
        'G': '\033[93m',
        'T': '\033[91m',
        'U': '\033[91m',
        'reset': '\033[0;0m'
    }

    tmpStr = ""

    for nuc in seq:
        if nuc in bcolors:
            tmpStr += bcolors[nuc] + nuc
        else:
            tmpStr += bcolors['reset'] + nuc

    return tmpStr + '\033[0;0m'

def parse_fasta_file(filename):
    with open(filename) as fp:
        Lines = fp.readlines()
        DNA_strings = {}
        for line in Lines:
            line = line.strip()
            if line.startswith(">"):
                string_id = line.replace(">", "")
                DNA_strings[string_id] = ""
            else:
                DNA_strings[string_id] += line

    return DNA_strings
